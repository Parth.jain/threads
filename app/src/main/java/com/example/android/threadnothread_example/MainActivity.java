package com.example.android.threadnothread_example;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Bitmap mBitmap;
    private ImageView mIView;
    private int mDelay = 5000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIView=findViewById(R.id.imageView);
        final Button loadbutton=findViewById(R.id.loadButton);
        loadbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadIcon();
            }
        });
        final Button otherButton=findViewById(R.id.otherButton);
        otherButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this,"Other Button working",Toast.LENGTH_LONG).show();

            }
        });
        
    }


    private void loadIcon() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(mDelay);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Log.e("Error", e.toString());
                }
                mBitmap=BitmapFactory.decodeResource(getResources(),R.drawable.painter);

                mIView.post(new Runnable() {
                    @Override
                    public void run() {

                        mIView.setImageBitmap(mBitmap);
                    }
                });
            }
        }).start();

    }
}
